package org.emerjoin.cdi.jersey.test.example.square;

import org.emerjoin.cdi.jersey.test.example.square.persistence.SquareEntity;

public class Square {

    private int id;
    private int height;
    private int width;

    public Square(){

    }

    public Square(int width, int height){
        this.width = width;
        this.height = height;
    }

    public Square(int id, int width, int height){
        this.id = id;
        this.width = width;
        this.height = height;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public SquareEntity toEntity(){

        return new SquareEntity(width,height);

    }

}
