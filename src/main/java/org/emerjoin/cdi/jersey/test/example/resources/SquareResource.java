package org.emerjoin.cdi.jersey.test.example.resources;

import org.apache.deltaspike.jpa.api.transaction.Transactional;
import org.emerjoin.cdi.jersey.test.example.square.Square;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/foo")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface SquareResource {

    @POST
    @Transactional
    @Produces(MediaType.TEXT_PLAIN)
    String add(Square square);

    @GET
    @Transactional
    List<Square> list();

}
