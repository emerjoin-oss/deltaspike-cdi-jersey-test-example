package org.emerjoin.cdi.jersey.test.example.square.persistence;


import org.emerjoin.cdi.jersey.test.example.square.Square;

import javax.persistence.*;

@Entity
@Table(name = "square")
public class SquareEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private int width;
    private int height;

    public SquareEntity(){

    }

    public SquareEntity(int width, int height){
        this.width = width;
        this.height = height;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Square toDto(){
        return new Square(id,width,height);
    }

}
