package org.emerjoin.cdi.jersey.test.example.resources;

import org.apache.deltaspike.jpa.api.transaction.Transactional;
import org.emerjoin.cdi.jersey.test.example.square.Square;
import org.emerjoin.cdi.jersey.test.example.square.persistence.SquareEntity;
import org.emerjoin.cdi.jersey.test.example.square.persistence.SquareJpaRepository;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.stream.Collectors;

@Path("/foo")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class SquareResourceImpl implements SquareResource {

    @Inject
    private SquareJpaRepository squareJpaRepository;


    @POST
    @Transactional
    @Produces(MediaType.TEXT_PLAIN)
    public String add(Square square){

        SquareEntity entity = square.toEntity();
        squareJpaRepository.save(entity);
        return String.valueOf(entity.getId());


    }


    @GET
    @Transactional
    public List<Square> list(){

        return squareJpaRepository.findAll().stream()
                .map(SquareEntity::toDto)
                .collect(Collectors.toList());

    }


}
