package org.emerjoin.cdi.jersey.test.example.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/foo")
@Produces(MediaType.TEXT_PLAIN)
public class FooResource {


    @GET
    @Path("/test")
    public String test(){

        return "Foo";

    }


}
