package org.emerjoin.cdi.jersey.test.example.resources;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/bar")
@Produces(MediaType.TEXT_PLAIN)
public class BarResource {

    @GET
    @Path("/test")
    public String test(){

        return "Bar";

    }

}
