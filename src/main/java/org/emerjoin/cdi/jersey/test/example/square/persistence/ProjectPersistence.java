package org.emerjoin.cdi.jersey.test.example.square.persistence;

import org.apache.deltaspike.jpa.api.entitymanager.PersistenceUnitName;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ProjectPersistence {

    private  EntityManagerFactory emf;

    private EntityManagerFactory getEmf(){
        if(emf==null)
            emf = Persistence.createEntityManagerFactory("sample-pu");
        return emf;
    }

    @Produces
    @RequestScoped
    public EntityManager produceEntityManager(){

        return getEmf().createEntityManager();

    }

}
