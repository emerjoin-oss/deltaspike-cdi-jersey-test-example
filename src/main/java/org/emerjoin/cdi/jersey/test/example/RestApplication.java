package org.emerjoin.cdi.jersey.test.example;


import org.emerjoin.cdi.jersey.test.example.resources.BarResource;
import org.emerjoin.cdi.jersey.test.example.resources.FooResource;
import org.emerjoin.cdi.jersey.test.example.resources.SquareResourceImpl;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/api")
@ApplicationScoped
public class RestApplication extends Application {

    public Set<Class<?>> getClasses() {
        Set<Class<?>> set = new HashSet<>();
        set.add(FooResource.class);
        set.add(BarResource.class);
        set.add(SquareResourceImpl.class);
        return set;
    }

}
