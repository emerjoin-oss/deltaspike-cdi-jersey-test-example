package org.emerjoin.cdi.jersey.test.example.square.persistence;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

@Repository(forEntity = SquareEntity.class)
public interface SquareJpaRepository extends EntityRepository<SquareEntity,Integer> {



}
