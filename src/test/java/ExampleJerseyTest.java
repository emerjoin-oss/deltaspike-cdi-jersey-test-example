import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.emerjoin.cdi.jersey.test.example.RestApplication;
import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.emerjoin.cdi.deltaspike.servlet.DeltaspikeCdiRequestContextFilter;
import org.emerjoin.cdi.jersey.test.example.resources.SquareResource;
import org.emerjoin.cdi.jersey.test.example.square.Square;
import org.glassfish.jersey.jackson.internal.jackson.jaxrs.json.JacksonJsonProvider;
import org.glassfish.jersey.servlet.ServletContainer;
import org.glassfish.jersey.test.DeploymentContext;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.grizzly.GrizzlyWebTestContainerFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.glassfish.jersey.test.ServletDeploymentContext;

import java.util.Collections;

@RunWith(CdiTestRunner.class)
public class ExampleJerseyTest extends JerseyTest {

    static
    {
        System.setProperty("jersey.config.test.container.factory", GrizzlyWebTestContainerFactory.class.getName());
    }


    protected DeploymentContext configureDeployment(){
        return ServletDeploymentContext.builder(RestApplication.class)
                .servletClass(ServletContainer.class)
                .initParam("javax.ws.rs.Application",RestApplication.class.getCanonicalName())
                .addFilter(DeltaspikeCdiRequestContextFilter.class,"cdi-servlet-request-producer-filter")
                .build();
    }

    private <T> T client(Class<T>  resourceType){
        return JAXRSClientFactory.create(target().getUri().toASCIIString(),
                resourceType,
                Collections.singletonList(
                        new JacksonJsonProvider()));
    }

    @Test
    public void foo_test() {

        String resp = target("/foo/test").request().get(String.class);
        Assert.assertEquals("foo test", "Foo", resp);

    }

    @Test
    public void bar_test() {

        String resp = target("/bar/test").request().get(String.class);
        Assert.assertEquals("bar test", "Bar", resp);

    }

    @Test
    public void square_add_and_list() {

        SquareResource squareResource = client(SquareResource.class);
        String squareId = squareResource.add(new Square(100,200));
        Assert.assertNotNull(squareId);
        Assert.assertEquals(1,squareResource.list().size());

    }



}
