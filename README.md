# Why
Cause we needed to unit test CDI-JAX-RS applications without having to use the all mighty Arquillian. It was a matter of simplicity, efficiency (test execution time) and out of the box debugging capability.

# Java-EE APIs used in this project
* CDI (with Weld)
* JAX-RS (with Jersey)
* JPA (with Hibernate and deltaspike-data)

# Considerations
In order to escape from the Ambiguous dependencies exception caused by deltaspike-data **BeanManagedUserTransactionStrategy** we explicitly excluded the bean in **beans.xml**, making using a WELD-specific XML Schema, thus giving on portability.